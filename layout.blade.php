<!doctype html>
<html lang="en">
	<head>
	<meta name="google-site-verification" content="K-CBekTqsgCBQHbmMuTwmMEtCJ4HWLUnBeEWo2Xt12k" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-179384225-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-179384225-1');
</script>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		
		<link href="https://fonts.googleapis.com/css?family=Mr+Dafoe" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://painting.indone.me/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="http://painting.indone.me/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/gabrez-inc/ceess/master/style.css">
    <link rel="manifest" href="https://musika.my.id/manifest.json">
    <link rel="apple-touch-icon" href="/favicon.ico" />
<meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="http://painting.indone.me/favicon.ico">
    <meta name="theme-color" content="#ffffff">
		<style type="text/css">
			html, body {
			    font-family: "Open Sans", Arial, sans-serif;
			    font-size: 14px;
			    font-weight: 300;
			    background: #f3f3f3 url(@yield('bg')) no-repeat center center fixed;
				background-size: cover;
				height: 100%;
			}

			#title{
				font-family: "Mr Dafoe", Times, serif;
			    font-size: 34px;
			    background: rgba(255, 255, 255, 0.8);
			    margin-bottom: 34px;
			}

			.header{
				padding-bottom: 13px;
			    margin-bottom: 13px;
			    border-bottom: 1px solid #eee;
			}

			a {
			    color: #26ADE4;
			    text-decoration: none;
			}

			#title a{
				color: #333;
				margin-left: 21px;
			}

			#title a:hover{
				text-decoration: none;
				color: #26ADE4;
			}

			.container{
				max-width: 960px;
				margin: 0px auto 0px;1
			    padding: 20px;
			    background-color: #fff;
			    border: 1px solid #ccc;
			    border-top: 1px solid #ddd;
			    border-left: 1px solid #ddd;
			    box-shadow: 3px 3px 3px rgba(150,150,150,0.2);
			}
			.sidebar ul{
				margin: 0;
			    padding: 0;
			}

			.sidebar ul li{
				list-style: none;
			}

			.footer{
				margin-top: 21px;
				padding-top: 13px;
				border-top: 1px solid #eee;
			}

			.footer a{
				margin: 0 15px;
			}

			.navi{
				margin:13px 0 13px 0;
				padding:13px;
			}

			.navi a{
				margin: 5px 2px;
				font-size: 95%;
			}
			
		</style>

		@yield('head')
		@include('header')
	</head>
	<body>
		<header class="bg-white fixed-top border-bottom">
        <div class="container py-2 ">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="logo text-center text-md-left">
                        <a href="https://apk-uwu.netlify.app/"/>APK UWU</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class='search-box'>
                        
<form class='search-form' action="https://www.google.com/search" class="searchform" method="get" name="searchform" target="_blank">
<input class='form-control' name="sitesearch" type="hidden" value="{{ home_url() }}">
<input class='form-control' autocomplete="on" name="q" placeholder="Search...." required="required" type="text" style="background-color: #ffffff; border: 0px;"></form>
<button class='btn btn-link search-btn' type="submit">
                                <i class='fa fa-search'></i>
                    </div>

                        
                    </div>
                </div>
            </div>
        </div>
        </div></div></div></div></div></div></div></div>
    </header>
<main class="main container border">
<div class="row header">
				<div class="col-sm-12 text-center"><br><br>
					@yield('header')
					@include('related')

				</div>
			</div>
			<div class="row content">
				<div class="col-md-12">
					
					@yield('content')
				</div></div></div>
			
            <h2 class="h5 font-weight-normal title-h"> Random Posts</h2>
			@foreach(collect(keywords())->shuffle()->take(20) as $keyword)
                <ul class="list-side border px-2">
<li><a href="{{ image_url($keyword) }}">
					{{ $keyword }}
					</a></li> </ul>
            

	@endforeach
	</div>
				
		</div>
			<footer class="py-4 mb-1 text-center container border-left border-right border-bottom">
        <div class="container py-2 text-center border-left border-right">
					@foreach(pages() as $page)
				 <a href="{{ page_url($page) }}">{{ ucwords(str_replace('-', ' ', $page)) }}</a>•
					@endforeach

				</div>
			</div>
		</div>
		@section('content')
	@if(isset($_GET['img']))
	<div class="row">
		<div class="col-md-12 text-center">
			<p><a href="{{ $_GET['img'] }}" class="btn btn-outline-primary" download="image">Download Image</a></p>
			<p><img src="{{ $_GET['img'] }}" alt="" class="img-fluid"></p>

		</div>
	</div>
	<div class="mt-3"></div>
	@endif

	@foreach(collect(keywords())->shuffle()->take(16)->chunk(4) as $chunked)
		<div class="row mt-3">
			@foreach($chunked as $keyword)
				<div class="col-md-3">
					<p class="text-center">{{ $keyword }}</p>
					<a href="{{ image_url($keyword) }}">
						<img src="{{ image_url($keyword, true) }}" alt="" class="img-fluid" onerror="this.onerror=null;this.src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQh_l3eQ5xwiPy07kGEXjmjgmBKBRB7H2mRxCGhv1tFWg5c_mWT';">
					</a>
				</div>
				
			@endforeach
			
		</div>

	@endforeach
@endsection

		@include('bar')
		@include('footer')
		<script type='text/javascript'>//<![CDATA[
function ignielLazyLoad(){eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('u B(){Y(v e=o.1r("B"),t=0;t<e.1q;t++)Q(e[t])&&(e[t].N=e[t].1p("1n-N"))}u Q(e){v t=e.1t();Z t.1x>=0&&t.1w>=0&&t.1v<=(y.1u||o.T.1m)&&t.1k<=(y.1c||o.T.1b)}v b=["\\r\\m\\m\\D\\G\\a\\f\\c\\M\\n\\p\\c\\a\\f\\a\\k","\\h\\f","\\r\\c\\c\\r\\l\\A\\D\\G\\a\\f\\c","\\g\\h\\r\\m","\\p\\l\\k\\h\\g\\g","\\V\\1a\\1e\\R\\h\\f\\c\\a\\f\\c\\M\\h\\r\\m\\a\\m","\\w\\p\\a\\1l\\p\\c\\k\\n\\l\\c","\\r","\\1f\\w\\a\\k\\L\\1j\\a\\g\\a\\l\\c\\h\\k\\W\\g\\g","\\g\\a\\f\\q\\c\\A","\\w\\p\\a\\k\\W\\q\\a\\f\\c","\\c\\a\\p\\c","\\m\\h\\l\\w\\F\\a\\f\\c\\D\\g\\a\\F\\a\\f\\c","\\1i\\h\\m\\L","\\l\\g\\n\\l\\1g","\\p\\l\\k\\h\\g\\g\\1h\\h\\J","\\c\\h\\J","\\q\\a\\c\\S\\h\\w\\f\\m\\n\\f\\q\\R\\g\\n\\a\\f\\c\\1z\\a\\l\\c","\\A\\k\\a\\X","\\a\\1y\\a\\l","\\q\\a\\c\\D\\g\\a\\F\\a\\f\\c\\S\\L\\1F\\m","\\p\\l\\k\\h\\g\\g\\U\\a\\n\\q\\A\\c","\\n\\f\\f\\a\\k\\U\\a\\n\\q\\A\\c","\\J\\k\\a\\G\\a\\f\\c\\V\\a\\X\\r\\w\\g\\c","\\n\\c\\a\\F"];u I(d,j){y[b[0]]?y[b[0]](d,j):y[b[2]](b[1]+d,j)}I(b[3],B),I(b[4],B),o[b[0]](b[5],u(){b[6];Y(v d=o[b[8]](b[7]),j=d[b[9]],s=/1D|1B/i[b[11]](1G[b[10]])?o[b[12]]:o[b[13]],C=u(d,j,s,C){Z(d/=C/2)<1?s/2*d*d*d+j:s/2*((d-=2)*d*d+2)+j};j--;){d[b[1C]](j)[b[0]](b[14],u(d){v j,E=s[b[15]],x=o[b[1A]](/[^#]+$/[b[19]](1H[b[18]])[0])[b[17]]()[b[16]],z=s[b[1d]]-y[b[1s]],O=z>E+x?x:z-E,K=1o,H=u(d){j=j||d;v x=d-j,z=C(x,E,O,K);s[b[15]]=z,K>x&&P(H)};P(H),d[b[1E]]()})}});',62,106,'||||||||||x65|_0x1b5d|x74|_0xdd48x2||x6E|x6C|x6F||_0xdd48x3|x72|x63|x64|x69|document|x73|x67|x61|_0xdd48x4||function|var|x75|_0xdd48x7|window|_0xdd48x8|x68|lazy|_0xdd48x5|x45|_0xdd48x6|x6D|x76|_0xdd48xb|registerListener|x70|_0xdd48xa|x79|x4C|src|_0xdd48x9|requestAnimationFrame|isInViewport|x43|x42|documentElement|x48|x44|x41|x66|for|return|||||||||||x4F|clientWidth|innerWidth|21|x4D|x71|x6B|x54|x62|x53|left|x20|clientHeight|data|900|getAttribute|length|getElementsByClassName|22|getBoundingClientRect|innerHeight|top|right|bottom|x78|x52|20|trident|24|firefox|23|x49|navigator|this'.split('|'),0,{}));} eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('j 4=["\\7\\9\\9\\e\\d\\a\\b\\8\\i\\g\\h\\8\\a\\b\\a\\k","\\f\\c\\7\\9","\\7\\8\\8\\7\\m\\l\\e\\d\\a\\b\\8","\\c\\b\\f\\c\\7\\9"];5[4[0]]?5[4[0]](4[1],6,!1):5[4[2]]?5[4[2]](4[1],6):5[4[3]]=6;5[4[0]]?5[4[0]](4[1],6,!1):5[4[2]]?5[4[2]](4[1],6):5[4[3]]=6;',23,23,'||||_0xdfb4|window|ignielLazyLoad|x61|x74|x64|x65|x6E|x6F|x76|x45|x6C|x69|x73|x4C|var|x72|x68|x63'.split('|'),0,{}));
//]]></script>
	</body>
</html>